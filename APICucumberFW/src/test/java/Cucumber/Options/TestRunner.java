package Cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith (Cucumber.class)
@CucumberOptions(features="src/test/java/Features",glue= {"StepDefinations"},tags="@PostApiTc or @PatchApiTc or @PutApiTc")
public class TestRunner {

}
