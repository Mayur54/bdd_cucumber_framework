package DataDrivenStepDefination;

import io.cucumber.java.en.Given;
import io.restassured.path.json.JsonPath;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PatchEndpoint;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.cucumber.java.en.*;

public class PatchAPI_DataDrivenStepDefination extends CommonMethodsHandleApi {
	String requestbody;
	int statuscode;
	String responsebody;
	File LogDir = ManageDirectory.CreateLogDirectory("PatchTestLogDir");
	String endpoint = PatchEndpoint.PatchEndpointTc1();

	@Given("Enter {string} and {string} in patch requestbody")
	public void enter_and_in_patch_requestbody(String req_name, String req_job) {
		// Write code here that turns the phrase above into concrete actions
		requestbody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Patchdatadriven request with Payload")
	public void send_the_patch_request_with_payload() throws IOException {
		statuscode = patch_statuscode(requestbody, endpoint);
		responsebody = patch_responsebody(requestbody, endpoint);
		System.out.println("Patch responsebody is " + responsebody);
		ManageApiLogs.evidence_creator(LogDir, "PatchTestClassDataDriven", endpoint, requestbody, responsebody);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Patchdatadriven status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statuscode, 200);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Patchdatadriven responsebody Parameters")
	public void validate_patch_responsebody_parameters() {
		JsonPath Jsp_req = new JsonPath(requestbody);
		String req_name = Jsp_req.getString("name");
		String req_job = Jsp_req.getString("job");
		JsonPath Jsp_res = new JsonPath(responsebody);
		String res_name = Jsp_res.getString("name");
		String res_job = Jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body of PatchAPI Validation Succesfull \n");
		// throw new io.cucumber.java.PendingException();
	}

}
