package DataDrivenStepDefination;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PostEndpoint;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class PostAPI_DataDrivenStepDefination extends CommonMethodsHandleApi {
	String requestbody;
	int statuscode;
	String responsebody;
	File LogDir = ManageDirectory.CreateLogDirectory("PostTestLogDir");
	String endpoint = PostEndpoint.PostEndpointTc1();

	@Given("Enter {string} and {string} in post requestBody")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
		// Write code here that turns the phrase above into concrete actions
		// RestAssured.baseURI = "https://reqres.in/";
		requestbody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		// requestbody = PostReqRepository.PostRequestTc1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the postdatadriven request with Payload")
	public void send_the_request_with_payload() throws IOException {
		statuscode = post_statusCode(requestbody, endpoint);
		responsebody = post_responsebody(requestbody, endpoint);
		System.out.println("Responsebody is : " + responsebody);
		ManageApiLogs.evidence_creator(LogDir, "PostTestClassDataDriven1", endpoint, requestbody, responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate postdatadriven status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate postdatadriven responsebody Parameters")
	public void validate_responsebody_parameters() {
		JsonPath jsprequest = new JsonPath(requestbody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responsebody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body Validation Successfull \n");
		// throw new io.cucumber.java.PendingException();
	}

}
