package DataDrivenStepDefination;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PutEndpoints;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.cucumber.java.en.*;
import io.restassured.path.json.JsonPath;

public class PutAPI_DataDrivenStepDefination extends CommonMethodsHandleApi {
	String requestbody;
	int statuscode;
	String responsebody;
	String endpoint = PutEndpoints.PutEndpointTc1();
	File LogDir = ManageDirectory.CreateLogDirectory("PutLogDir");

	@Given("Enter {string} and {string} in Put requestbody")
	public void enter_and_in_put_requestbody(String req_name, String req_job) {
		requestbody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Putdatadriven request with Payload")
	public void send_the_put_request_with_payload() throws IOException {
		statuscode = put_statuscode(requestbody, endpoint);
		responsebody = put_responsebody(requestbody, endpoint);
		System.out.println("Put responsebody is " + responsebody);
		ManageApiLogs.evidence_creator(LogDir, "PutTestClassDataDriven", endpoint, requestbody, responsebody);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Putdatadriven statuscode")
	public void validate_put_statuscode() {
		Assert.assertEquals(statuscode, 200);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Putdatadriven responsebody Parameters")
	public void validate_put_responsebody_parameters() {
		JsonPath Jsp_req = new JsonPath(requestbody);
		String req_name = Jsp_req.getString("name");
		String req_job = Jsp_req.getString("job");
		JsonPath Jsp_res = new JsonPath(responsebody);
		String res_name = Jsp_res.getString("name");
		String res_job = Jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body of PatchAPI Validation Succesfull");
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

}
