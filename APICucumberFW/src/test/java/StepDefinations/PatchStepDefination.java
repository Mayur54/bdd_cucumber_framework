package StepDefinations;

import io.cucumber.java.en.Given;
import io.restassured.path.json.JsonPath;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PatchEndpoint;
import ReqRepository.PatchReqRepository;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

public class PatchStepDefination extends CommonMethodsHandleApi{
	String requestbody;
	int statuscode;
	String responsebody;
	File LogDir = ManageDirectory.CreateLogDirectory("PatchTestLogDir");
	String endpoint = PatchEndpoint.PatchEndpointTc1();
	
	@Before
	public void setup() {
		System.out.println("\n-------APi Triggered-------");
	}
	@After
	public void teardown() {
		System.out.println("-------Execution Successfull-------");
	}
	@Given("Enter NAME and JOB in patch requestbody")
	public void enter_name_and_job_in_patch_requestbody() throws IOException {
	    // Write code here that turns the phrase above into concrete actions
		requestbody = PatchReqRepository.PatchReqRepositoryTc1();
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the Patch request with Payload")
	public void send_the_patch_request_with_payload() throws IOException {
		statuscode = patch_statuscode(requestbody,endpoint);
		responsebody=patch_responsebody(requestbody,endpoint);
		System.out.println("Patch responsebody is "+responsebody);
		ManageApiLogs.evidence_creator(LogDir, "PatchTestClass1", endpoint, requestbody, responsebody);
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statuscode, 200);
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Patch responsebody Parameters")
	public void validate_patch_responsebody_parameters() {
	    JsonPath Jsp_req = new JsonPath(requestbody);
	    String req_name = Jsp_req.getString("name");
	    String req_job = Jsp_req.getString("job");
	    JsonPath Jsp_res = new JsonPath(responsebody);
	    String res_name = Jsp_res.getString("name");
	    String res_job = Jsp_res.getString("job");
	    Assert.assertEquals(res_name,req_name);
	    Assert.assertEquals(res_job,req_job);
	    System.out.println("Response Body of PatchAPI Validation Succesfull");
	   // throw new io.cucumber.java.PendingException();
	}


}
