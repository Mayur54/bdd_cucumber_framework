package StepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PutEndpoints;
import ReqRepository.PutReqRepository;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.cucumber.java.en.*;
import io.restassured.path.json.JsonPath;

public class PutAPI_StepDefination extends CommonMethodsHandleApi{
	String requestbody;
	int statuscode;
	String responsebody;
	String endpoint= PutEndpoints.PutEndpointTc1();
	File LogDir= ManageDirectory.CreateLogDirectory("PutLogDir");
	
	@Given("Enter NAME and JOB in Put requestbody")
	public void enter_name_and_job_in_put_requestbody() throws IOException {
		requestbody=PutReqRepository.PutReqRepositoryTc1();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the Put request with Payload")
	public void send_the_put_request_with_payload() throws IOException {
		statuscode = put_statuscode(requestbody,endpoint);
		responsebody =put_responsebody(requestbody,endpoint);
		System.out.println("Put responsebody is "+responsebody);
		ManageApiLogs.evidence_creator(LogDir, "PutTestClass1", endpoint, requestbody, responsebody);
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Put statuscode")
	public void validate_put_statuscode() {
		Assert.assertEquals(statuscode, 200);
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Put responsebody Parameters")
	public void validate_put_responsebody_parameters() {
		JsonPath Jsp_req = new JsonPath(requestbody);
	    String req_name = Jsp_req.getString("name");
	    String req_job = Jsp_req.getString("job");
	    JsonPath Jsp_res = new JsonPath(responsebody);
	    String res_name = Jsp_res.getString("name");
	    String res_job = Jsp_res.getString("job");
	    Assert.assertEquals(res_name,req_name);
	    Assert.assertEquals(res_job,req_job);
	    System.out.println("Response Body of PatchAPI Validation Succesfull");
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}

}
