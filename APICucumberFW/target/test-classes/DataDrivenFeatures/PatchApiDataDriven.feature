Feature: Triger Patch Api

Scenario Outline: Triger the Patch API with valid request parameters
        Given Enter "<NAME>" and "<JOB>" in patch requestbody
        When Send the Patchdatadriven request with Payload
        Then Validate Patchdatadriven status code
        And Validate Patchdatadriven responsebody Parameters
        
Examples:
         |NAME|JOB|
         |Pooja|QA|
         |Nikul|Mech|
         |Ankit|CS|        