Feature: Trigger post API

Scenario Outline: Trigger the Post API with valid request parameters
       Given Enter "<NAME>" and "<JOB>" in post requestBody 
       When Send the postdatadriven request with Payload
       Then Validate postdatadriven status code 
       And Validate postdatadriven responsebody Parameters
       
Examples:
       |NAME|JOB|    
       |Mayur|Eng| 
       |Aishwarya|Comp|
       |Samarth|CS|