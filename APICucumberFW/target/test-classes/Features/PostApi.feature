Feature: Trigger post API
@PostApiTc
Scenario: Trigger the Post API with valid request parameters
       Given Enter NAME and JOB in post requestBody 
       When Send the post request with Payload
       Then Validate post status code 
       And Validate post responsebody Parameters