# BDD_Cucumber_Framework


Welcome to the BDD Cucumber Framework repository! This framework is built for Behavior-Driven Development (BDD) using Cucumber, incorporating data-driven step definitions and common methods for handling status codes and response bodies.


 

**Features :-**

- **Cucumber Integration:** Utilize Cucumber with Gherkin syntax for writing expressive feature files.

- **Data-Driven Steps:** Leverage data-driven step definitions for flexibility and reusability.

- **Tags:** Organize and execute specific scenarios using Cucumber tags.

- **CommonMethods Package:** Centralize status code and response body handling for easy maintenance.

In this Framework we have libraries which we are using , they are :
1) JUnit - which provides various anotations and assertions to facilitate the writing and execution of tests.
2) Cucumber-java To include java library,
3) Cucumber-junit To run Cucumber tests with JUnit as the test runner,
4) RestAssured to triger the api ,extract the responebody and status code,
5) JsonPath To parse the responsebody,
6) Apache-Poi to read and write from an excel file, 
7) TestNG libraries for validating responsebody parameters by using assert.
